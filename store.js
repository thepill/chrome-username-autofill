
class SitesStore {
    load(callback) {
        chrome.storage.local.get({"sites" : []}, function(result) {
            callback(result.sites)
        })
    }

    save(sites, callback) {
        chrome.storage.local.set({"sites" : sites}, function() {
            callback()
        })
    }

    clear() {
        chrome.storage.local.clear()
    }
}