# chrome-autofill-username

A chrome extension to autofill username inputs on websites without using the chrome password manager.


## Install

1. `git clone https://github.com/thepill/chrome-autofill-username`
2. Navigate to `chrome://extensions` in your browser. 
3. Check the box next to `Developer_Mode`.
4. Click `Load_Unpacked_Extension` and select the cloned directory
