let sitesList = document.querySelector('#sites tbody')
let store = new SitesStore()

document.querySelector('#clearAll').addEventListener('click', function(){
    store.clear()
    updateShownSitesFromStore()
})

document.querySelector('#export').addEventListener('click', () => {
    exportSites()
})

document.querySelector('#import').addEventListener('click', () => {
    let _importContainer = document.querySelector('#import-container')
    _importContainer.classList.add('active')

    document.querySelector('#import-start').addEventListener('click', () => {
        let data = document.querySelector('#import-data').value
        importSites(JSON.parse(data))
        _importContainer.classList.remove('active')
    })
})

function updateShownSitesFromStore() {
    sitesList.innerHTML = ''
    store.load((sites) => {
        sites.forEach(site => {
            let row = createTableRow(site)
            sitesList.appendChild(row)
        });
    })
}

function createTableRow(site) {
    let row = document.createElement('tr')

    let siteColumn = document.createElement('td')
    siteColumn.innerText = site.url 
    row.appendChild(siteColumn)

    let usernameColumn = document.createElement('td')
    usernameColumn.innerText = site.username 
    row.appendChild(usernameColumn)

    let selectorColumn = document.createElement('td')
    selectorColumn.innerText = site.inputSelector 
    row.appendChild(selectorColumn)

    let deleteBtn = document.createElement('button')
    deleteBtn.setAttribute('data-id', site.id);
    deleteBtn.innerText = "Delete"
    deleteBtn.classList.add('delete')
    deleteBtn.onclick = deleteEntry
    row.appendChild(deleteBtn)

    return row
}

function deleteEntry(e) {
    let id = e.target.getAttribute('data-id');

    store.load((sites) => {
        let filterdSites = sites.filter(s => s.id != id)

        store.save(filterdSites, () => {
            updateShownSitesFromStore()
        })
    })
}

function exportSites() {
    store.load((sites) => {
        var exp = JSON.stringify(sites);

        var url = 'data:application/json;base64,' + btoa(exp);
        chrome.downloads.download({
            url: url,
            filename: 'username-autofill-sites.json'
        });
    })
}

function importSites(sites) {
    store.save(sites, () => {
        updateShownSitesFromStore()
    })
}

updateShownSitesFromStore()