let _url = document.querySelector('#url')
let _username = document.querySelector('#username')
let _inputSelector = document.querySelector('#custom-input-selector')
let _options = document.querySelector('#go-to-options')
let _message = document.querySelector('#message')
let store = new SitesStore()

chrome.tabs.query({ active: true, currentWindow: true }, (tabs) => {
    let currentTab = tabs[0]
    var url = new URL(currentTab.url)
    var domain = url.hostname
    _url.value = domain
 });

_options.addEventListener('click', function(e) {
    e.preventDefault();
    chrome.runtime.openOptionsPage()
 });

 document.querySelector('#add').addEventListener('click', function() {
    let url = _url.value
    let username = _username.value
    let inputSelector = _inputSelector.value

    store.load((sites) => {
        let exists = sites.find(s => s.url === url && s.username === username)
        
        if(exists) {
            _message.innerText = 'Combination of site and username already exisits!'
            _message.classList.add('show')
            return
        }

        sites.push({
            id: Date.now(), 
            url, 
            username,
            inputSelector
        })
    
        store.save(sites, () => {
            _message.innerText = 'Added! Reload the page for a test.'
            _message.classList.add('show')
        })
    })
 })


