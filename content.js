
new SitesStore().load((sites) => {
    sites.forEach(element => {
        let reg = new RegExp(element.url)

        if(location.href.match(reg)) {
            let _input = element.inputSelector ? document.querySelector(element.inputSelector) : document.querySelector('input[name=username i]')

            if(_input !== null && _input.value === '') {
                _input.value = element.username
                _input.focus()
                document.querySelector('input[type=password i]').focus()
            }
        }
    })
})
